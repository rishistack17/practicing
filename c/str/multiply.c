#include <stdio.h>
#include <string.h>

int strToInt(char n[200]){
	int res=0;
	for (int i = 0; i < 200; ++i)
	{
		if (n[i]==0)
		{
			break;
		}else{
			res *=10;
			res+=(n[i]-48);
		}
	 	
	}
	return res;
}
void solve(int n1, int n2)
{
	int res = n1*n2;
	printf("%d\n",res );
}
int main(int argc, char const *argv[])
{
	char n1[200];
	char n2[200];
	scanf("%s",n1);
	scanf("%s",n2);
	int num1 = strToInt(n1);
	int num2 = strToInt(n2);
	solve(num1,num2);
	return 0;
}
