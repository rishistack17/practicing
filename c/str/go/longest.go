package main

// sum passed all test cses
//refer q of url == https://leetcode.com/problems/longest-substring-without-repeating-characters/
import "fmt"

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

//return true if a char is repeated else retuen false
func check(s string) bool {
	res := false
	freq := make(map[interface{}]int)
	for _, val := range s {
		if freq[val] >= 1 {
			res = true
			break
		}
		freq[val]++
	}
	return res
}
func solve(s string) int {
	res := 0
	if s != "" {
		res = 1
		for i := 0; i < len(s); i++ {
			// if res <= 1 {
			for j := len(s) - 1; j > i; j-- {
				if !check(s[i:j]) {
					res = max(res, j-i)
					break
				}
			}
			// }
			// break
		}
	}
	return res
}
func main() {
	fmt.Println(solve(""))
}
