package main
//url :https://leetcode.com/problems/longest-palindromic-substring/
import "fmt"


//return true if a char is repeated else retuen false
func check(s string) bool {
	res := false
	freq := make(map[interface{}]int)
	for _, val := range s {
		if freq[val] >= 1 {
			res = true
			break
		}
		freq[val]++
	}
	return res
}
func solve(s string) string {
	res :=string(s[0])
	if len(s)> 1 {
		for i := 0; i < len(s); i++ {
			// if res <= 1 {
			for j := len(s) - 1; j > i; j-- {
				if check(s[i:j]) {
					res =s[i:j]
					break
				}
			}
			// }
			// break
		}
	}
	return res
}


func main() {
	fmt.Println(solve("babad"))
}
