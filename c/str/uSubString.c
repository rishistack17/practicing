#include <stdio.h>

int uniqueSubstring( char s[100000])
{
	int c=0;
	int n=0;
	int dummy[123]={0};
	for (int i = 0; i < 100000; ++i)
	{
		if (s[i]==0){
			break;
		}
		dummy[s[i]]++;
		n++;
	}
	for (int i = 0; i < 123; ++i)
	{
		if (dummy[i]>1)
			c++;
	}

	
	return n-c;
		
}

int permute(int n)
{
	if (n<=1){
		return 1;
	}
	return n*permute(n-1);
}
int main(int argc, char const *argv[])
{
	char s[100000];
	scanf("%s",s);
	// uniqueSubstring(s);
	printf("%d\n",permute(uniqueSubstring(s)));
	return 0;
}